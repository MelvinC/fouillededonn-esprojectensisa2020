import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math as m
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from tqdm import tqdm
import scipy.io.wavfile as sci_wav
from sklearn import preprocessing
from sklearn import tree
from sklearn . metrics import precision_score

# lecture des données

df=pd.read_csv('D:\\Documents\\ENSISA\\Fouille de données\\Projet\\fouillededonn-esprojectensisa2020\\dogs_and_cat.csv', header=0)

def read_audio(file):
    if not isinstance(file, list):
        file = [file]
    return [sci_wav.read('D:\\Documents\\ENSISA\\Fouille de données\\Projet\\fouillededonn-esprojectensisa2020\\cats_dogs\\' + f)[1] for f in file]

print(read_audio('cat_1.wav'))
print(read_audio(['cat_1.wav', 'cat_2.wav']))

df.head()

couleurs = []
#for f in df.iloc[:,3]:
#    if isinstance(f, str):
#        x_train.append({"train_cat" : read_audio(f)})
#X_train = pd.DataFrame(x_train)
X_train = []
Y_train = []
for f in tqdm(df.iloc[:,3]):
    tmp2 = []
    if isinstance(f, str):
        tmp = read_audio(f)
        for j in range (len(tmp[0])):
            tmp2.append(tmp[0][j])
            if(m.isnan(tmp[0][j])):
                print("prout")
        X_train.append(np.array(tmp2))
        couleurs.append(1)
        Y_train.append("cat")
for f in tqdm(df.iloc[:,4]):
    tmp2 = []
    if isinstance(f, str):
        tmp = read_audio(f)
        for j in range (len(tmp[0])):
            tmp2.append(tmp[0][j])
            if(m.isnan(tmp[0][j])):
                print("prout")
        X_train.append(np.array(tmp2))
        couleurs.append(2)
        Y_train.append("dog")
# print("X_train :")
# #print(X_train)

# Y_train = []
# for f in tqdm(df.iloc[:,3]):
#     tmp2 = []
#     if isinstance(f, str):
#         tmp = read_audio(f)
#         for j in range (len(tmp[0])):
#             tmp2.append(tmp[0][j])
#             if(m.isnan(tmp2[j])):
#                 print("prout")
#         Y_train.append(tmp2)
# for f in tqdm(df.iloc[:,4]):
#     tmp2 = []
#     if isinstance(f, str):
#         tmp = read_audio(f)
#         for j in range (len(tmp[0])):
#             tmp2.append(tmp[0][j])
#             if(m.isnan(tmp2[j])):
#                 print("prout")
#         Y_train.append(tmp2)
# 
# #
X_test = []
Y_test = []
for f in tqdm(df.iloc[:,1]):
    tmp2 = []
    if isinstance(f, str):
        tmp = read_audio(f)
        for j in range (len(tmp[0])):
            tmp2.append(tmp[0][j])
        X_test.append(np.array(tmp2))
        Y_test.append(1)
for f in tqdm(df.iloc[:,2]):
    tmp2 = []
    if isinstance(f, str):
        tmp = read_audio(f)
        for j in range (len(tmp[0])):
            tmp2.append(tmp[0][j])
        X_test.append(np.array(tmp2))
        Y_test.append(2)
#X_test = pd.DataFrame(x_test).values
print("X_test :")
x_train=pd.DataFrame(X_train)
x_train=x_train.fillna(0)
X_test=pd.DataFrame(X_test)
X_test=X_test.fillna(0)
x_test=X_test.iloc[:,:x_train.shape[1]]
Y_test=pd.DataFrame(Y_test)

print(x_train.shape, x_test.shape)
##x_test=np.array(X_test)
#print(X_test.values)
#print(X_test)

# Y_test = []
# for f in tqdm(df.iloc[:,1]):
#     tmp2 = []
#     if isinstance(f, str):
#         tmp = read_audio(f)
#         for j in range (len(tmp[0])):
#             tmp2.append(tmp[0][j])
#         Y_test.append(tmp2)
# for f in tqdm(df.iloc[:,2]):
#     tmp2 = []
#     if isinstance(f, str):
#         tmp = read_audio(f)
#         for j in range (len(tmp[0])):
#             tmp2.append(tmp[0][j])
#         Y_test.append(tmp2)








# Plus proches voisins

# attributes = list(df.columns.values)[:-1]
# 
# def distEucl(l1, l2):
#     d = 0
#     if (len(l1)<len(l2)):
#         mini = l1
#         maxi = l2
#     else :
#         mini = l2
#         maxi = l1
#     for i in range (len(mini)):
#         d = d + ((int(mini[i])-int(maxi[i]))**2)
#     #print("d : ", d)
#     return m.sqrt(d)
# 
# # recherche du plus proche voisin
# print("len(Y_test) : ", len(Y_test))
# print("len(X_test) : ", len(X_test))
# prediction = np.zeros(len(X_test), dtype='object')
# print(prediction)
# 
# for i in tqdm(range (len(X_test))):
#     distMin = np.inf
#     indexMin = -1
#     current = X_test[i]
#     for j in range (len(X_train)):
#         t = X_train[j]
#         #print("len(current) : ")
#         #print(len(current))
#         #print("len(t) : ")
#         #print(len(t))
#         dist = distEucl(current, t)
#         if dist < distMin:
#             distMin = dist
#             indexMin = j
#     prediction[i] = Y_train[indexMin]
# 
# # évaluation du classifieur
# 
# cmap = plt.cm.get_cmap("Set1", len(np.unique(couleurs))+1)
# 
# for i,ts in tqdm(enumerate(X_train)):
#   #plt.plot([i for i in range(0,len(ts))],ts,c=cmap(Y_train[i]))
#   plt.plot([i for i in range(0,len(ts))],ts, c=cmap(couleurs[i]))
# 
# plt.legend(['class 1', 'class 2'])
# plt.show()
# 
# error = 0
# for i in tqdm(range(1,len(X_test))):
#     series = X_test[i]
#     minDist = float('inf')
#     index = -1
#     for j in range(1,len(X_train)):
#         tmp = X_train[j]
#         dist = distEucl(series, tmp)
#         if(dist < minDist):
#             minDist = dist
#             index = j
#     if couleurs[i] != couleurs[index]:
#         error = error + 1
# 
# print('Precision: '+str(round(error / len(X_test),3)))
# 
# a = X_train[0]
# b = X_train[1]
# 
# 
# # def dtw(s1, s2):
# #     print("len(s1) : ", len(s1))
# #     print("len(s2) : ", len(s2))
# #     DTW = np.zeros((len(s1)+1,len(s2)+1))
# #     DTW[:, 0] = np.inf
# #     DTW[0, :] = np.inf 
# #     DTW[0, 0] = 0
# # 
# #     for i in range(1, DTW.shape[0]):
# #         for j in range(1, DTW.shape[1]):
# #             dist= (s1[i-1]-s2[j-1])**2
# #             DTW[i, j] = dist + min(DTW[i-1, j], DTW[i, j-1], DTW[i-1, j-1])
# #     return math.sqrt(DTW[len(s1), len(s2)]), DTW
# # 
# # for i in tqdm(range(1,len(X_test))):
# #     series = X_test[i]
# #     minDist = float('inf')
# #     index = -1
# #     for j in range(1,len(X_train)):
# #         tmp = X_train[j]
# #         dist, mtx  = dtw(series, tmp)
# #         if(dist < minDist):
# #             minDist = dist
# #             index = j
# # 
# # def plot_cost_matrix(a,b,mtx):
# #   fig = plt.figure(figsize=(6, 6))
# #   grid = plt.GridSpec(4, 4, hspace=0.2, wspace=0.2)
# #   main_ax = fig.add_subplot(grid[:-1, 1:])
# #   y_ts = fig.add_subplot(grid[:-1, 0], xticklabels=[])
# #   x_ts = fig.add_subplot(grid[-1, 1:], yticklabels=[], sharex=main_ax)
# # 
# #   main_ax.imshow(mtx, origin='lower', cmap='gray', interpolation='nearest')
# # 
# #   y_ts.plot(a,[i for i in range(0,len(ts))])
# #   y_ts.invert_xaxis()
# # 
# #   x_ts.plot([i for i in range(0,len(ts))],b)
# #   x_ts.invert_yaxis()
# #   plt.show()
# # 
# # plot_cost_matrix(a,b,mtx)


# euclidean distance
def eucdist(a, b):
    dist = 0
    for index, x in np.ndenumerate(a):
        dist += (b[index] - x)**2
    return m.sqrt(dist)

# matplotlib
cmap = plt.cm.get_cmap("Set1", len(np.unique(couleurs))+1)
for i,ts in enumerate(x_train.values):
  plt.plot([i for i in range(0,len(ts))],ts,c=cmap(couleurs[i]))

plt.legend(['class 1', 'class 2'])
plt.show()

error = 0
for i in tqdm(range(1,(x_test.values).shape[0])):
    series = x_test.values[i,:]
    minDist = float('inf')
    index = -1
    for j in range(1,(x_train.values).shape[0]):
        tmp = x_train.values[j,:]
        dist = eucdist(series, tmp)
        if(dist < minDist):
            minDist = dist
            index = j
    if Y_test.values[i] != couleurs[index]:
        error = error + 1

print('Precision: '+str(round(error / (x_test.values).shape[0],3)))









# Arbre de décision

#dummies = [pd.get_dummies(df[c]) for c in df.drop('Play golf', axis =1).columns]
dummies = [pd.get_dummies(df[c]) for c in df.columns]
binary_data = pd.concat(dummies , axis=1)

X = binary_data.values

le = preprocessing.LabelEncoder ()
y = le.fit_transform(Y_train)

clf = tree.DecisionTreeClassifier ()
print("Y_train : ", np.array(couleurs))
print("X_train : ", x_train)
clf = clf.fit(x_train, np.array(couleurs))

y_pred = clf.predict(x_test)
accuracy = precision_score(Y_test, y_pred)
print ('DecisionTreeClassifier accuracy score : {} '.format(accuracy))